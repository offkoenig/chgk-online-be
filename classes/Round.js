const md5 = require('md5');
const {ObjectId} = require('bson');
const mongoose = require('mongoose');

const roundSchema = require('../schema/round');

class Round {
  /** @type {ObjectId} */
	_id;

  /** @type {ObjectId} */
	gameId;

	/**
	 * @type {{
	 *  roundTime: Number,
	 *  questionTime: Number,
	 *  answerTime: Number
	 * }}
	 */
	settings;

  /** @type {Boolean} */
	removed;

  /**
   * Найти раунд по id
   * @param {String} id
   * @returns {Promise<Player>}
   */
   static async findById(id) {
    const result = await Model.findOne({'_id': id});

    return result;
  }

  /**
   * Найти раунд по id игры
   * @param {String} gameId
   * @returns {Promise<Player>}
   */
   static async findByGameId(gameId) {
    const result = await Model.findOne({'gameId': gameId});

    return result;
  }

  /**
   * Создать раунд
   * @param {String} gameId
   * @param {{
	 *  roundTime: Number,
	 *  questionTime: Number,
	 *  answerTime: Number
   * }} settings
   * @returns {Promise<Player>}
   */
  static async create(gameId, settings = {
    roundTime: 60,
    questionTime: 50,
    answerTime: 10
  }) {
    const round = new Model({
      gameId,
      settings
    });

    round.save();
    return round;
  }
}

const Schema = new mongoose
  .Schema(roundSchema, {
    toJSON    : {
      getters : true,
      virtuals: true,
    },
    timestamps: true
  })
  .loadClass(Round);

const Model = new mongoose
  .model('Round', Schema);

module.exports = {
  Round: Model
}
