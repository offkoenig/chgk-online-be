const express = require('express');
const cors = require('cors');
const md5 = require('md5');

const config = require('./../config');
const { Logger } = require('./../utils/Logger');
const { User } = require('./User');
const { Player } = require('./Player');
const { Game } = require('./Game');

class Response {
  constructor(success, data) {
    this.success = success;
    this.data = data;
  }
}

class API {
  /** @type {API} */
  static instance = null;

  static initialize() {
    return new API();
  }

  constructor() {
    this.app = express();

    this.app.use(cors());
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: true }));
    this.listen();

    this.app.listen(config.expressPort, () => Logger.success(`API сервер успешно запущен на порту ${config.expressPort}`));

    API.instance = this;
  }

  /**
   * Middleware
   * @param {Express.Request} req
   * @param {Express.Response} res
   * @param {void} next
   */
  async middleware(req, res, next) {
    const { authorization } = req.headers;
    if (!authorization) {
      Logger.error(`Неверный токен: ${authorization}`);
      res.json(new Response(false, 'No token provided'));
      return;
    }

    next();
  }

  /**
   * Listening API requests
   */
  listen() {
    this.app.get(
      '/api/handshake',
      this.middleware.bind(this),
      (...args) => this.onUserPersonal(...args)
    );

    this.app.post(
      '/api/auth',
      (...args) => this.onAuth(...args)
    );

    this.app.post(
      '/api/register',
      (...args) => this.onRegister(...args)
    );

    this.app.get(
      '/api/user',
      this.middleware.bind(this),
      (...args) => this.onUserPersonal(...args)
    );

    this.app.post(
      '/api/game/create',
      this.middleware.bind(this),
      (...args) => this.onGameCreate(...args)
    );

    this.app.post(
      '/api/game/join',
      this.middleware.bind(this),
      (...args) => this.onGameJoin(...args)
    );

    this.app.post(
      '/api/game/start',
      this.middleware.bind(this),
      (...args) => this.onGameStart(...args)
    );

    this.app.get(
      '/api/game/list',
      (...args) => this.onGamesList(...args)
    );
  }

  /**
   * Получить информацию о пользователе
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  async onUserPersonal(req, res) {
    const { authorization } = req.headers;

    const user = await User.findByToken(authorization);
    if (!user) {
      Logger.error(`Не найден юзер с токеном: ${authorization}`);
      return res.json(new Response(false, 'incorrect token'));
    }

    return res.json(new Response(true, {
      login: user.info.login,
      name: user.info.name
    }));
  }

  /**
   * Handshake event
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  async onHandshake(req, res) {
    const { authorization } = req.headers;
    const user = await User.findByToken(authorization);
    if (!user) {
      Logger.error('Кто-то стучится с некорректным токеном');
      return res.json(new Response(false, 'incorrect token'));
    }

    Logger.log(`${user.info.name} пожал руку`);
    res.json(new Response(true, 'handshake success'));
  }

  /**
   * Регистрация
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  async onRegister(req, res) {
    const {
      login,
      password,
      name
    } = req.body;

    const user = await User.findByLogin(login);
    if (user) {
      return res.json(new Response(false, 'User already registered'));
    }

    const newUser = await User.register(login, password, name);

    Logger.log(`${newUser.info.name} зарегистрировался`);
    res.json(new Response(true, newUser.access_token));
  }

  /**
   * Авторизация
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  async onAuth(req, res) {
    const {
      login,
      password
    } = req.body;

    const user = await User.findByLogin(login);
    if (!user) {
      return res.json(new Response(false, 'User not found'));
    }

    if (md5(password) !== user.info.password) {
      return res.json(new Response(false, 'Password incorrect'));
    }

    const token = await User.generateToken(login);

    Logger.log(`${user.info.name} авторизовался`);
    res.json(new Response(true, token));
  }

  /**
   * Создать игру
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  async onGameCreate(req, res) {
    const { authorization } = req.headers;
    const { name, settings } = req.body;
    const user = await User.findByToken(authorization);
    if (!user) {
      return res.json(new Response(false, 'Bad token'));
    }

    const game = await Game.create(name);
    const player = await Player.create(user._id, game._id, 1);

    Logger.success(`Создана игра ${game.info.name}`);
    Logger.success(`Создан игрок`);

    res.json(new Response(true, {
      game,
      player
    }))
  }

  /**
   * Получить список игр
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
  async onGamesList(req, res) {
    const gamesList = await Game.find();

    res.json(new Response(true, gamesList))
  }

  /**
   * Вступить в игру
   * @param {Express.Request} req
   * @param {Express.Response} res
   */
   async onGameJoin(req, res) {
      const { authorization } = req.headers;
      const { gameId } = req.body;

      const game = await Game.findById(gameId);
      if (!game) {
        return res.json(new Response(false, 'No such game'));
      }

      const user = await User.findByToken(authorization);
      if (!user) {
        return res.json(new Response(false, 'Bad token'));
      }

      const player = await Player.findByGameId(game._id);
      if (player) {
        res.json(new Response(true, {
          game,
          player
        }));
        return;
      }

      const newPlayer = await Player.create(user._id, game._id, 0);
      Logger.success(`Создан игрок`);

      res.json(new Response(true, {
        game,
        newPlayer
      }));
    }

    /**
     * Начать игру
     * @param {Express.Request} req
     * @param {Express.Response} res
     */
     async onGameStart(req, res) {
      const { authorization } = req.headers;
      const { gameId } = req.body;

      const user = await User.findByToken(authorization);
      if (!user) {
        return res.json(new Response(false, 'Bad token'));
      }

      const game = await Game.findById(gameId);
      if (!game) {
        return res.json(new Response(false, 'No such game'));
      }

      const player = await Player.findByGameId(game._id);
      if (!player) {
        res.json(new Response(false, 'Player not found'));
        return;
      }

      if (player.info.status !== 1) {
        res.json(new Response(false, 'Player is not admin'));
        return;
      }

      const result = await game.start();

      res.json(new Response(true, result));
     }
}

module.exports = {
  API
}