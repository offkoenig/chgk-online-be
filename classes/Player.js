const md5 = require('md5');
const {ObjectId} = require('bson');
const mongoose = require('mongoose');

const playerSchema = require('../schema/player');

class Player {
  /** @type {ObjectId} */
	_id;

  /** @type {ObjectId} */
	userId;

  /** @type {ObjectId} */
	gameId;

  /** @type {Boolean} */
	removed;

  /**
   * Найти игрока по id
   * @param {String} id
   * @returns {Promise<Player>}
   */
  static async findById(id) {
    const result = await Model.findOne({'_id': id});

    return result;
  }

  /**
   * Найти игрока по id пользователя
   * @param {String} userId
   * @returns {Promise<Player>}
   */
  static async findByUserId(userId) {
    const result = await Model.findOne({'userId': userId});

    return result;
  }

  /**
   * Найти игрока по id игры
   * @param {String} gameId
   * @returns {Promise<Player>}
   */
  static async findByGameId(gameId) {
    const result = await Model.findOne({'gameId': gameId});

    return result;
  }

  /**
   * Создать игрока
   * @param {String} userId
   * @param {String} gameId
   * @returns {Promise<Player>}
   */
  static async create(userId, gameId, status = 0) {
    const player = new Model({
      userId,
      gameId,
      info: {
        status
      }
    });

    player.save();
    return player;
  }
}

const Schema = new mongoose
  .Schema(playerSchema, {
    toJSON    : {
      getters : true,
      virtuals: true,
    },
    timestamps: true
  })
  .loadClass(Player);

const Model = new mongoose
  .model('Player', Schema);

module.exports = {
  Player: Model
}