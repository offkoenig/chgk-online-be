const md5 = require('md5');
const {ObjectId} = require('bson');
const mongoose = require('mongoose');

const userSchema = require('../schema/user');


class User extends mongoose.Model {
  /** @type {ObjectId} */
	_id;

	/**
	 * @type {{
	 *  login: String,
	 *  password: String,
	 *  name: String
	 * }}
	 */
	info;

  /** @type {String} */
  access_token;

  /**
   * Генерирует access_token
   * @param {string} login
   * @returns {string}
   */
  static async generateToken(login) {
    const token = md5(`${Math.random()*1000}${login}${Date.now()}`);

    const user = await User.findByLogin(login);
    if (!user) {
      return token;
    }

    user.access_token = token;
    await user.save();

    return token;
  }

  /**
	 * Находит пользователя по логину
	 * @param {string} login
	 * @returns {Promise<User>}
	 */
	static async findByLogin(login) {
		return await Model.findOne({'info.login': login});
	}

  /**
	 * Находит пользователя по токену
	 * @param {string} token
	 * @returns {Promise<User>}
	 */
	static async findByToken(token) {
		return await Model.findOne({'access_token': token});
	}

  /**
   * Найти пользователя по id
   * @param {string} _id
   * @returns {Promise<User>}
   */
  static async findById(_id) {
    return await Model.findOne({'_id': _id});
  }

  /**
   * Зарегистрировать нового пользователя
   * @param {string} login
   * @param {string} password
   * @param {string} name
   * @returns {Promise<User>}
   */
  static async register(login, password, name) {
    const user = new Model({
      info: {
        login: login,
        password: md5(password),
        name: name
      },
      access_token: await User.generateToken(login)
    });

    await user.save();
    return user;
  }
}

const Schema = new mongoose
  .Schema(userSchema, {
    toJSON    : {
      getters : true,
      virtuals: true,
      transform: function (doc, ret, options) {
        delete ret.access_token;

        return ret;
      }
    },
    timestamps: true
  })
  .loadClass(User);

const Model = new mongoose
  .model('User', Schema);

module.exports = {
  User: Model
}