const md5 = require('md5');
const {ObjectId} = require('bson');
const mongoose = require('mongoose');

const gameSchema = require('../schema/game');
const { Round } = require('./Round');

class Game {
  /** @type {ObjectId} */
	_id;

	/**
	 * @type {{
	 *  name: String,
	 * }}
	 */
	info;

	/**
	 * @type {{
	 *  duration: Number,
	 *  maxPlayers: Number,
	 *  minPlayers: Number,
   *  isPublic: Boolean
	 * }}
	 */
	settings;

  /** @type {Boolean} */
	removed;


  /**
   * Найти игру по id
   * @param {String} id
   * @returns {Promise<Player>}
   */
   static async findById(id) {
    const result = await Model.findOne({'_id': id});

    return result;
  }

  /**
   * Создать игру
   * @param {String} name
   * @param {{
	 *  duration: Number,
	 *  maxPlayers: Number,
	 *  minPlayers: Number
   * }} settings
   * @returns {Promise<Player>}
   */
  static async create(name, settings = {}) {
    const game = new Model({
      info: {
        name
      },
      settings
    });

    game.save();
    return game;
  }

  static async remove(id) {
    const game = Game.findById(id);
    if (!game) {
      return;
    }

    game.removed = true;
    game.save();
    return game;
  }

  async start() {
    const round = await Round.create(this._id);

    return round;
  }
}

const Schema = new mongoose
  .Schema(gameSchema, {
    toJSON    : {
      getters : true,
      virtuals: true,
    },
    timestamps: true
  })
  .loadClass(Game);

const Model = new mongoose
  .model('Game', Schema);

module.exports = {
  Game: Model
}
