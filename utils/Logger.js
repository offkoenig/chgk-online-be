const chalk = require("chalk");

class Logger {
  static timestamp = new Date();
  static hours = '0' + Logger.timestamp.getHours();
  static minutes = '0' + Logger.timestamp.getMinutes();
  static seconds = '0' + Logger.timestamp.getSeconds();

  static cTime = `[${Logger.hours.substr(-2)}:${Logger.minutes.substr(-2)}:${Logger.seconds.substr(-2)}]`;

  static log(message) {
    console.log(`${chalk.grey.bold(Logger.cTime)} ${message}`);
  }

  static success(message) {
    console.log(`${chalk.bold.greenBright(Logger.cTime)} ${chalk.greenBright(message)}`);
  }

  static error(message) {
    console.log(`${chalk.bgRedBright.bold(Logger.cTime)}`, chalk.red(`ERROR | ${message}`))
  }
}

module.exports = { Logger };