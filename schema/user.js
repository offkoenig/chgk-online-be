module.exports = {
  info: {
    login: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true
    },
    name: {
      type: String,
      default: 'NO_NAME'
    }
  },
  access_token: {
    type: String,
    required: true
  }
}