const {ObjectId} = require('bson');

module.exports = {
  gameId: {
    type: ObjectId,
    required: true
  },
  settings: {
    roundTime: {
      type: Number,
      required: true
    },
    questionTime: {
      type: Number,
      default: 60
    },
    answerTime: {
      type: Number,
      default: 10
    }
  },
  removed: {
    type: Boolean,
    default: false
  }
}