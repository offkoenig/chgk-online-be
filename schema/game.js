module.exports = {
  info: {
    name: {
      type: String,
      default: 'NO_NAME'
    },
  },
  settings: {
    duration: {
      type: Number,
      default: 24
    },
    maxPlayers: {
      type: Number,
      default: 10
    },
    minPlayers: {
      type: Number,
      default: 2
    },
    isPublic: {
      type: Boolean,
      default: true
    }
  },
  removed: {
    type: Boolean,
    default: false
  }
}