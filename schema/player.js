const {ObjectId} = require('bson');

module.exports = {
  userId: {
    type: ObjectId,
    required: true
  },
  gameId: {
    type: ObjectId,
    default: null
  },
  removed: {
    type: Boolean,
    default: false
  },
  info: {
    status: {
      type: Number,
      default: 0
    }
  }
}