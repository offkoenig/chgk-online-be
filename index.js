const mongoose = require('mongoose');
const chalk = require('chalk');

const { API } = require('./classes/API');
const { Logger } = require('./utils/Logger');
const { User } = require('./classes/User');
const { Player } = require('./classes/Player');
const { Game } = require('./classes/Game');
const { Round } = require('./classes/Round');

(async () => {
  Logger.log('Подключение к БД...');
	await mongoose.connect(`mongodb://127.0.0.1:27017/chgk`);
  const users = await User.find();
  const players = await Player.find();
  const games = await Game.find();
  const rounds = await Round.find();
  Logger.success('Подключение к БД успешно');
  Logger.log(`Найдено ${chalk.greenBright(users.length)} пользователей`);
  Logger.log(`Найдено ${chalk.greenBright(players.length)} игроков`);
  Logger.log(`Найдено ${chalk.greenBright(games.filter(v => !v.removed).length)} игр`);
  Logger.log(`Найдено ${chalk.greenBright(rounds.length)} раундов`);

  Logger.log('Запуск API сервера...');
  API.initialize();
})().catch(err => Logger.error(err));